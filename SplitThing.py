#!/bin/python3
import re

print("Gib input")

text = input()
letters = re.findall(r"[\d]+", text)
# ~ letters = text.lower()
print(letters)

keyboard_set = ["q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m"]
text_out = ""

for letter in letters:
	place = letters.index(letter) + 1
	print(place)
	key = int(letter) / int(place)
	print(key)
	character = keyboard_set[int(key) - 1]
	print(character)
	
	text_out += character
	
print(text_out)


# ~ place = 0

# ~ for letter in letters:
    
    # ~ try:
        # ~ char_value = keyboard_set.index(letter) + 1
    # ~ except:
        # ~ continue
    
    # ~ place += 1
    # ~ print("Place: " + str(place))
    
    # ~ print("Char Value: " + str(char_value))
    # ~ number = place * char_value
    # ~ print("Number: " + str(number))
    
    # ~ text_out += str(number) + " "
    
# ~ print(text_out)
